# APP Mobile City 7

## Requirements
- npm or yarn
- React Native CLI
- Android SDK setup, with emulator or device attached. See https://facebook.github.io/react-native/docs/getting-started.html

Should work on Windows, Linux or Mac.

## First steps

Install dependencies
```sh
$ npm install
```
After that, we'll need to [link the native dependencies](https://facebook.github.io/react-native/docs/linking-libraries-ios).
```sh
$ react-native link
```

## Running the APP
Running on Android (first you'll need to have a AVD running):
```sh
$ react-native run-android
```

Running on iOS:
```sh
$ react-native run-ios
```