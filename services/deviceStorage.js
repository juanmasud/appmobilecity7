// import { AsyncStorage } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

const deviceStorage = {
  async saveKey (key, valueToSave) {
    try {
      await AsyncStorage.setItem(key, valueToSave)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async loadToken () {
    try {
      const value = await AsyncStorage.getItem('id_token')
      if (value !== null) {
        this.setState({
          token: value,
          loading: false
        })
      } else {
        this.setState({
          token: null,
          loading: false
        })
      }
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async deleteToken () {
    try {
      await AsyncStorage.removeItem('id_token')
        .then(
          () => {
            this.setState({
              token: null
            })
          }
        )
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  }

}

export default deviceStorage
