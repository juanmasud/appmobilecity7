/**
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { AppRegistry } from 'react-native'

import Login from './screens/Login'
import Secured from './screens/Secured'
import deviceStorage from './services/deviceStorage'

export default class App extends Component {
  constructor () {
    super()
    this.state = {
      token: null,
      loading: true
    }

    this.newToken = this.newToken.bind(this)
    this.deleteToken = deviceStorage.deleteToken.bind(this)
    this.loadToken = deviceStorage.loadToken.bind(this)
    this.loadToken()
  }

  newToken (token) {
    this.setState({
      token: token
    })
  }

  render () {
    if (this.state.token) {
      return <Secured deleteToken={this.deleteToken} />
    } else {
      return <Login newToken={this.newToken} />
    }
  }
}
AppRegistry.registerComponent(App, () => App)
